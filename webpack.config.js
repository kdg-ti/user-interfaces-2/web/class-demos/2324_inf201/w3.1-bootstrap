import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";


const config = {
  devtool: "source-map",
  mode: "development",
  /**
   * Wil je andere entrypoints, dan index.ts? Dat kan zo:
   *  entry: {
   *     home: path.resolve('./home.ts'),
   *     about:  path.resolve('./about.ts'),
   *     contact:  path.resolve('./contact.js'),
   *   },
   */
  resolve: {
    extensions: [".ts",".js"],
    extensionAlias: {
      '.js': ['.js', '.ts'],
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve("src/html/index.html"),
    }),
    new HtmlWebpackPlugin({
      filename: "nav.html",
      template: path.resolve("src/html/nav.html"),
    }),
    new HtmlWebpackPlugin({
      filename: "navbar.html",
      template: path.resolve("src/html/navbar.html"),
    }),
    new HtmlWebpackPlugin({
      filename: "form.html",
      template: path.resolve("src/html/form.html"),
    }),
    new MiniCssExtractPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.ts$/i,
        use: ["ts-loader"],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        // voor het verschil kan je hier kijken https://maxrozen.com/difference-between-style-loader-mini-css-extract-plugin
        // use: ["style-loader", "css-loader"] // dit is soms makkelijker voor development
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.html?$/i,
        use: ["html-loader"],
      },
      // Image assets
      {
        test: /\.(png|svg|jpe?g|gif)$/i,
        type: "asset",
      },
      // Font assets
      {
        test: /\.(woff2?|eot|ttf|otf)$/i,
        type: "asset",
      },
    ],
  },
  devServer: {
    static: {directory: path.resolve("dist")},
    open: true,
    hot: false // must be disabled for liveReload
  },
  output: {
    // Maakt de 'dist' folder leeg alvorens nieuwe files te genereren.
    clean: true,
  },

  /**
   *  Het "output" veld kan verder gebruikt worden om de webpack defaults (output folder: 'dist', filename voor bundle: 'main.js') aan te passen
   *  naar je eigen voorkeuren (bv. 'build' wordt ook veel gebruikt)
   *
   *  */

  /* output: {
         filename: '[name].bundle.js',
         path: path.resolve('./dist'),
         clean: true,
     },*/
};

export default config;